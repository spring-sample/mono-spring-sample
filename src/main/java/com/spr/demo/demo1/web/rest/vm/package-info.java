/**
 * View Models used by Spring MVC REST controllers.
 */
package com.spr.demo.demo1.web.rest.vm;
