package com.spr.demo.demo1;

import com.spr.demo.demo1.Demo1App;
import com.spr.demo.demo1.config.TestSecurityConfiguration;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Base composite annotation for integration tests.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@SpringBootTest(classes = { Demo1App.class, TestSecurityConfiguration.class })
public @interface IntegrationTest {
}
